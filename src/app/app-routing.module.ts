import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './layouts';
import { SignInComponent } from './features/sign-in/sign-in.component';

const routes: Routes = [
  {
    path: 'login',
    component: SignInComponent,
  },
  {
    path: 'home',
    component: MainLayoutComponent,
    children: [
      {
        path: 'danh-sach-quan-ly-nhan-vien',
        loadChildren: () =>
          import(
            './features/menu/employ-management/employ-management.module'
          ).then((m) => m.EmployManagementModule),
      },
      {
        path: 'danh-sach-thong-tin-sua-chua',
        loadChildren: () =>
          import('./features/menu/fix-information/fix-information.module').then(
            (m) => m.EmployManagementModule
          ),
      },
      {
        path: 'ho-so-may-moc',
        loadChildren: () =>
          import('./features/menu/machine-report/machine-report.module').then(
            (m) => m.MachineReportModule
          ),
      },
      {
        path: 'danh-sach-bao-cao-thong-ke',
        loadChildren: () =>
          import(
            './features/menu/statistical-report/statistical-report.module'
          ).then((m) => m.StatisticalReportModule),
      },
      {
        path: 'danh-sach-hoa-don-mua-linh-kien',
        loadChildren: () =>
          import('./features/menu/bill-report/bill-report.module').then(
            (m) => m.BillReportModule
          ),
      },
    ],
  },
  {
    path: '**',
    redirectTo: 'login',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
