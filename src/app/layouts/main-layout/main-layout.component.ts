import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatInputModule } from '@angular/material/input';
import { CommonModule } from '@angular/common';
import { MenuBarComponent } from '@app/shared/component';

@Component({
  standalone: true,
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  imports: [
    RouterModule,
    MatSidenavModule,
    MatInputModule,
    MenuBarComponent,
    CommonModule,
  ],
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent {}
