import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { Router, RouterLink } from '@angular/router';
import { AuthenticationService } from '@app/shared/services/authentication.service';

@Component({
  standalone: true,
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.scss'],
  imports: [
    MatIconModule,
    RouterLink,
    CommonModule,
    MatButtonModule,
    MatIconModule,
  ],
})
export class MenuBarComponent {
  user$ = this.authService.currentUser$;

  constructor(
    public authService: AuthenticationService,
    private router: Router
  ) {}

  logout() {
    this.authService.logout().subscribe(() => {
      this.router.navigate(['loggin']);
    });
  }
}
