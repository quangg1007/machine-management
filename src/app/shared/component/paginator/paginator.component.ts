import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatPaginatorModule, PageEvent } from '@angular/material/paginator';

@Component({
  standalone: true,
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  imports: [MatPaginatorModule],
  styleUrls: ['./paginator.component.scss'],
})
export class PaginatorComponent {
  @Input() length = 0;
  @Input() pageSize = 10;
  @Output() doSelectPage = new EventEmitter<PageEvent>();

  /**
   * Select page event
   * @param e
   */
  onSelectPage(e: PageEvent) {
    this.doSelectPage.emit(e);
  }

  // onPageChange(event: PageEvent) {
  //   const startIndex = event.pageIndex * event.pageSize;
  //   const endIndex = startIndex + event.pageSize;
  //   const slicedData = this.items.slice(startIndex, endIndex);
  //   this.dataSource.data = slicedData;
  // }
}
