import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {
  AddBillReportFormView,
  BillReportListView,
  EditBillReportFormView,
} from './view';

const routes: Routes = [
  {
    path: '',
    component: BillReportListView,
  },
  {
    path: 'them-moi',
    component: AddBillReportFormView,
  },
  {
    path: 'chinh-sua',
    component: EditBillReportFormView,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BillReportRoutingModule {}
