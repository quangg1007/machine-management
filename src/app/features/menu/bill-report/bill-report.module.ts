import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { PaginatorComponent } from '@app/shared/component';
import { BillReportRoutingModule } from './bill-report-routing.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  AddBillReportFormContainer,
  BillReportListContainer,
  EditBillReportFormContainer,
} from './container';
import {
  BillReportListView,
  AddBillReportFormView,
  EditBillReportFormView,
} from './view';

const containers = [
  AddBillReportFormContainer,
  BillReportListContainer,
  EditBillReportFormContainer,
];
const views = [
  BillReportListView,
  AddBillReportFormView,
  EditBillReportFormView,
];
const components: any[] = [];

@NgModule({
  declarations: [...containers, ...views, ...components],
  imports: [
    BillReportRoutingModule,
    ReactiveFormsModule,
    CommonModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatTableModule,
    MatMenuModule,
    MatRadioModule,
    FormsModule,
    PaginatorComponent,
  ],
})
export class BillReportModule {}
