import { BillReportType } from '../models/menu.models';

export interface BillReportState {
  bill: BillReportType[];
}

export const BILL_REPORT_DATA: BillReportState = {
  bill: [
    {
      id: 0,
      billNumber: 'Redesign Brief 2019.pdf',
      productName: 'abcxyz',
      dateOfSelling: '08 Jan 2019',
      productCode: 'abcxyz',
      quantity: 1,
      unitPrice: 100000,
      totalMoney: 100000,
      button: '',
    },
    {
      id: 1,
      billNumber: 'hello',
      productName: 'ksadjf',
      dateOfSelling: '08 Jan 2019',
      productCode: 'abcxyz',
      quantity: 2,
      unitPrice: 100000,
      totalMoney: 200000,
      button: '',
    },
  ],
};
