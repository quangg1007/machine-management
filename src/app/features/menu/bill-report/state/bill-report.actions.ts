import { createAction, props } from '@ngrx/store';
import { BillReportType } from '../models/menu.models';

export const ADD_BILL_REPORT_ACTION = '[bill report page] add bill';
export const UPDATE_BILL_REPORT_ACTION = '[bill report page] update bill';
export const DELETE_BILL_REPORT_ACTION = '[bill report page] delete bill';

export const addBillReport = createAction(
  ADD_BILL_REPORT_ACTION,
  props<{ bill: BillReportType }>()
);

export const updateBillReport = createAction(
  UPDATE_BILL_REPORT_ACTION,
  props<{ bill: BillReportType }>()
);

export const deleteBillReport = createAction(
  DELETE_BILL_REPORT_ACTION,
  props<{ id: number }>()
);
