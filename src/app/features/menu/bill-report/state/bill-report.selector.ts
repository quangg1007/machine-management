import { createFeatureSelector, createSelector } from '@ngrx/store';
import { BillReportState } from './bill-report.state';

const getBillReportState = createFeatureSelector<BillReportState>('bill');

export const getBillReport = createSelector(getBillReportState, (state) => {
  return state.bill;
});
