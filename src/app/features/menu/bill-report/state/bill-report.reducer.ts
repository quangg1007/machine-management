import { createReducer, on } from '@ngrx/store';
import { BILL_REPORT_DATA } from './bill-report.state';
import {
  addBillReport,
  deleteBillReport,
  updateBillReport,
} from './bill-report.actions';

const _billReducer = createReducer(
  BILL_REPORT_DATA,
  on(addBillReport, (state, action) => {
    let billReports = { ...action.bill };
    let lastIndex: number | any;
    lastIndex = state.bill[state.bill.length - 1].id;
    billReports.id = lastIndex + 1;
    return {
      ...state,
      bill: [...state.bill, billReports],
    };
  }),
  on(updateBillReport, (state, action) => {
    const updateBill = state.bill.map((bill) => {
      return action.bill.id === bill.id ? action.bill : bill;
    });
    return {
      ...state,
      bill: updateBill,
    };
  }),
  on(deleteBillReport, (state, action) => {
    const updatePosts = state.bill.filter((post) => {
      return post.id !== action.id;
    });
    console.log(state);
    return {
      ...state,
      bill: updatePosts,
    };
  })
);

export function billReducer(state: any, action: any) {
  return _billReducer(state, action);
}
