import { Component } from '@angular/core';
import { LocationStrategy } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '@app/features/menu/store/app.state';
import { BillReportType } from '../../models/menu.models';
import { updateBillReport } from '../../state/bill-report.actions';
@Component({
  selector: 'app-edit-bill-report-form-container',
  templateUrl: './edit-bill-report-form.container.html',
  styleUrls: ['./edit-bill-report-form.container.scss'],
})
export class EditBillReportFormContainer {
  billForm: FormGroup;
  totalPrice: number = 0;

  constructor(
    private location: LocationStrategy,
    private fb: FormBuilder,
    private store: Store<AppState>
  ) {
    const billInfo = this.location.getState() as BillReportType;
    console.log(billInfo);
    this.totalPrice = billInfo.totalMoney;
    this.billForm = this.fb.group({
      id: billInfo.id,
      billNumber: [billInfo.billNumber, [Validators.required]],
      productName: [billInfo.productName, [Validators.required]],
      dateOfSelling: [billInfo.dateOfSelling, [Validators.required]],
      productCode: [billInfo.productCode, [Validators.required]],
      quantity: [billInfo.quantity, [Validators.required]],
      unitPrice: [billInfo.unitPrice, [Validators.required]],
      totalMoney: billInfo.totalMoney,
      button: [billInfo.button, [Validators.required]],
    });

    const quantityControl = this.billForm.get('quantity');
    const unitControl = this.billForm.get('unitPrice');
    console.log(quantityControl, unitControl);
    if (quantityControl && unitControl) {
      console.log('abc');
      quantityControl.valueChanges.subscribe({
        next: (valueForm) => {
          console.log(valueForm);
          this.billForm.value.totalMoney =
            valueForm * this.billForm.value.unitPrice;
          this.totalPrice = valueForm * this.billForm.value.unitPrice;
        },
      });

      unitControl.valueChanges.subscribe({
        next: (valueForm) => {
          console.log(valueForm);
          this.billForm.value.totalMoney =
            this.billForm.value.quantity * valueForm;
          this.totalPrice = this.billForm.value.quantity * valueForm;
        },
      });
    }
  }
  back() {
    this.location.back();
  }

  onFormSubmit(): void {
    const billInfo: BillReportType = {
      id: this.billForm.value.id,
      billNumber: this.billForm.value.billNumber,
      productName: this.billForm.value.productName,
      dateOfSelling: this.billForm.value.dateOfSelling,
      productCode: this.billForm.value.productCode,
      quantity: this.billForm.value.quantity,
      unitPrice: this.billForm.value.unitPrice,
      totalMoney: this.billForm.value.totalMoney,
      button: this.billForm.value.button,
    };
    this.store.dispatch(updateBillReport({ bill: billInfo }));
    this.location.back();
  }

  onFormClear(): void {
    this.billForm.reset();
    this.totalPrice = 0;
  }

  showNameError(prop: string) {
    const nameForm = this.billForm.get(prop);
    if (nameForm?.touched && !nameForm.valid) {
      if (nameForm?.errors?.['required']) {
        return `${this.returnMessage(prop)} bắt buộc `;
      }
      if (nameForm?.errors?.['minlength']) {
        return `${this.returnMessage(prop)} nên có tối thiểu từ 10 kí tự`;
      }
    }
    return;
  }

  returnMessage(message: string): string {
    switch (message) {
      case 'billNumber': {
        return 'Mã số hóa đơn';
        break;
      }
      case 'productName': {
        return 'Tên sản phẩm';
        break;
      }
      case 'dateOfSelling': {
        return 'Ngày bán hàng';
        break;
      }
      case 'productCode': {
        return 'Mã sản phẩm';
        break;
      }
      case 'quantity': {
        return 'Số lượng sản phẩm';
        break;
      }
      case 'unitPrice': {
        return 'Đơn giá sản phẩm';
        break;
      }
      default: {
        return '';
      }
    }
  }
}
