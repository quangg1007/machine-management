import { Component, Input } from '@angular/core';
import { BILL_REPORT_DATA } from '../../state/bill-report.state';
import { BillReportType } from '../../models/menu.models';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getBillReport } from '../../state/bill-report.selector';
import { deleteBillReport } from '../../state/bill-report.actions';

@Component({
  selector: 'app-bill-report-list-container',
  templateUrl: './bill-report-list.container.html',
  styleUrls: ['./bill-report-list.container.scss'],
})
export class BillReportListContainer {
  @Input() dataSource: BillReportType[];
  billReportInfo: Observable<BillReportType[]>;

  constructor(private _router: Router, private store: Store) {
    this.dataSource = BILL_REPORT_DATA.bill;
    this.billReportInfo = this.store.select(getBillReport);
    this.billReportInfo.subscribe((val) => {
      this.dataSource = val;
    });
  }

  billInfo(index: number): BillReportType | any {
    return {
      id: this.dataSource[index].id,
      billNumber: this.dataSource[index].billNumber,
      productName: this.dataSource[index].productName,
      dateOfSelling: this.dataSource[index].dateOfSelling,
      productCode: this.dataSource[index].productCode,
      quantity: this.dataSource[index].quantity,
      unitPrice: this.dataSource[index].unitPrice,
      totalMoney: this.dataSource[index].totalMoney,
      button: this.dataSource[index].button,
    };
  }

  routeToEdit(id: number): void {
    console.log(this.billInfo(id));
    this._router.navigateByUrl(
      '/home/danh-sach-hoa-don-mua-linh-kien/chinh-sua',
      {
        state: this.billInfo(id),
      }
    );
  }

  deleteBillReport(id: number) {
    if (confirm('Bạn có chắc xóa thông tin!')) {
      console.log('delete the post');
    }

    this.store.dispatch(deleteBillReport({ id: id }));
  }

  displayedColumns: string[] = [
    'billNumber',
    'productName',
    'dateOfSelling',
    'productCode',
    'quantity',
    'unitPrice',
    'totalMoney',
    'button',
  ];
}
