import { Component } from '@angular/core';
import { LocationStrategy } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/features/menu/store/app.state';
import { Store } from '@ngrx/store';
import { BillReportType } from '../../models/menu.models';
import { addBillReport } from '../../state/bill-report.actions';
@Component({
  selector: 'app-add-bill-report-form-container',
  templateUrl: './add-bill-report-form.container.html',
  styleUrls: ['./add-bill-report-form.container.scss'],
})
export class AddBillReportFormContainer {
  billForm: FormGroup;
  quantity: number = 0;
  unitPrice: number = 0;
  totalMoney: number = 0;

  constructor(
    private location: LocationStrategy,
    private fb: FormBuilder,
    private store: Store<AppState>
  ) {
    this.billForm = this.fb.group({
      billNumber: ['', [Validators.required]],
      productName: ['', [Validators.required]],
      dateOfSelling: ['', [Validators.required]],
      productCode: ['', [Validators.required]],
      quantity: [0, [Validators.required]],
      unitPrice: [0, [Validators.required]],
    });

    this.billForm.valueChanges.subscribe({
      next: (valueForm) => {
        this.totalMoney = valueForm.quantity * valueForm.unitPrice;
      },
    });
  }

  back() {
    this.location.back();
  }

  getTotal() {
    return this.billForm.value['quantity'] * this.billForm.value['unitPrice'];
  }

  onFormSubmit(): void {
    const billInfo: BillReportType = {
      billNumber: this.billForm.value.billNumber,
      productName: this.billForm.value.productName,
      dateOfSelling: this.billForm.value.dateOfSelling,
      productCode: this.billForm.value.productCode,
      quantity: this.billForm.value.quantity,
      unitPrice: this.billForm.value.unitPrice,
      totalMoney: this.totalMoney,
      button: this.billForm.value.button,
    };

    this.store.dispatch(addBillReport({ bill: billInfo }));
    this.location.back();
  }

  onFormClear(): void {
    this.billForm.reset();
  }

  showNameError(prop: string) {
    const nameForm = this.billForm.get(prop);
    if (nameForm?.touched && !nameForm.valid) {
      if (nameForm?.errors?.['required']) {
        return `${this.returnMessage(prop)} bắt buộc `;
      }
      if (nameForm?.errors?.['minlength']) {
        return `${this.returnMessage(prop)} nên có tối thiểu từ 10 kí tự`;
      }
    }
    return;
  }

  returnMessage(message: string): string {
    switch (message) {
      case 'billNumber': {
        return 'Mã số hóa đơn';
        break;
      }
      case 'productName': {
        return 'Tên sản phẩm';
        break;
      }
      case 'dateOfSelling': {
        return 'Ngày bán hàng';
        break;
      }
      case 'productCode': {
        return 'Mã sản phẩm';
        break;
      }
      case 'quantity': {
        return 'Số lượng sản phẩm';
        break;
      }
      case 'unitPrice': {
        return 'Đơn giá sản phẩm';
        break;
      }
      default: {
        return '';
      }
    }
  }
}
