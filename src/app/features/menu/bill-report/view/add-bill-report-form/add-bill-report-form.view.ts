import { Component } from '@angular/core';

@Component({
  selector: 'app-add-bill-report-form-view',
  templateUrl: './add-bill-report-form.view.html',
  styleUrls: ['./add-bill-report-form.view.scss'],
})
export class AddBillReportFormView {}
