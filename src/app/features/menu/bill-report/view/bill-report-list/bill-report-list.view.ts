import { Component } from '@angular/core';

@Component({
  selector: 'app-bill-report-list-view',
  templateUrl: './bill-report-list.view.html',
  styleUrls: ['./bill-report-list.view.scss'],
})
export class BillReportListView {}
