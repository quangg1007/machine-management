import { Component } from '@angular/core';

@Component({
  selector: 'app-edit-bill-report-form-view',
  templateUrl: './edit-bill-report-form.view.html',
  styleUrls: ['./edit-bill-report-form.view.scss'],
})
export class EditBillReportFormView {}
