export interface BillReportType {
  id?: number;
  billNumber: string;
  productName: string;
  dateOfSelling: string;
  productCode: string;
  quantity: number;
  unitPrice: number;
  totalMoney: number;
  button: string;
}
