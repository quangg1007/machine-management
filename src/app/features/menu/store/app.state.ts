import { EmployState } from '../employ-management/state/employ-managment.state';
import { employeeReducer } from '../employ-management/state/employ-management.reducer';
import { FixInformationState } from '../fix-information/state/fix-information.state';
import { fixInfoReducer } from '../fix-information/state/fix-information.reducer';
import { machineInfoState } from '../machine-report/state/machine-report.state';
import { machineReportReducer } from '../machine-report/state/machine-report.reducer';
import { staticReportState } from '../statistical-report/state/statistical-report.state';
import { BillReportState } from '../bill-report/state/bill-report.state';
import { billReducer } from '../bill-report/state/bill-report.reducer';
import { statisticalInfoReducer } from '../statistical-report/state/statistical-report.reducer';

export interface AppState {
  employ: EmployState;
  info: FixInformationState;
  machine: machineInfoState;
  report: staticReportState;
  bill: BillReportState;
}

export const appReducer = {
  employ: employeeReducer,
  info: fixInfoReducer,
  machine: machineReportReducer,
  bill: billReducer,
  report: statisticalInfoReducer,
};
