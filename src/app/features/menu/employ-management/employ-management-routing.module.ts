import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {
  EditEmployManagementFormView,
  AddEmployManagementFormView,
  EmployManagementListView,
} from './view';

const routes: Routes = [
  {
    path: '',
    component: EmployManagementListView,
  },
  {
    path: 'chinh-sua',
    component: EditEmployManagementFormView,
  },
  {
    path: 'them-moi',
    component: AddEmployManagementFormView,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployManagementRoutingModule {}
