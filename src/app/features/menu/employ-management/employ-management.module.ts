import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { PaginatorComponent } from '@app/shared/component';
import { EmployManagementRoutingModule } from './employ-management-routing.module';
import {
  AddEmployManagementFormContainer,
  EditEmployManagementFormContainer,
  EmployManagementListContainer,
} from './container';
import {
  EditEmployManagementFormView,
  AddEmployManagementFormView,
  EmployManagementListView,
} from './view';
import { ReactiveFormsModule } from '@angular/forms';

const containers = [
  EmployManagementListContainer,
  EditEmployManagementFormContainer,
  AddEmployManagementFormContainer,
];
const views = [
  EditEmployManagementFormView,
  AddEmployManagementFormView,
  EmployManagementListView,
];
const components: any[] = [];

@NgModule({
  declarations: [...containers, ...views, ...components],
  imports: [
    CommonModule,
    EmployManagementRoutingModule,
    ReactiveFormsModule,
    PaginatorComponent,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatInputModule,
    MatSelectModule,
  ],
})
export class EmployManagementModule {}
