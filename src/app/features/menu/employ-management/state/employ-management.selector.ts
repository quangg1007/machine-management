import { createFeatureSelector, createSelector } from '@ngrx/store';
import { EmployState } from './employ-managment.state';

const getEmployeeState = createFeatureSelector<EmployState>('employ');

export const getEmployee = createSelector(getEmployeeState, (state) => {
  return state.employ;
});
