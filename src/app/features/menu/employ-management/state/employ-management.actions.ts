import { createAction, props } from '@ngrx/store';
import { employType } from '../models/menu.models';

export const ADD_EMPLOYEE_ACTION = '[posts page] add post';
export const UPDATE_EMPLOYEE_ACTION = '[posts page] update post';
export const DELETE_EMPLOYEE_ACTION = '[posts page] delete post';

export const addEmploy = createAction(
  ADD_EMPLOYEE_ACTION,
  props<{ employ: employType }>()
);

export const updateEmploy = createAction(
  UPDATE_EMPLOYEE_ACTION,
  props<{ employ: employType }>()
);

export const deleteEmploy = createAction(
  DELETE_EMPLOYEE_ACTION,
  props<{ id: number }>()
);
