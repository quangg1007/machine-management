import { createReducer, on } from '@ngrx/store';
import { EMPLOYEE_DATA } from './employ-managment.state';
import {
  addEmploy,
  updateEmploy,
  deleteEmploy,
} from './employ-management.actions';

const _employeeReducer = createReducer(
  EMPLOYEE_DATA,
  on(addEmploy, (state, action) => {
    let employee = { ...action.employ };
    let lastIndex: number | any;
    lastIndex = state.employ[state.employ.length - 1].id;
    employee.id = lastIndex + 1;
    return {
      ...state,
      employ: [...state.employ, employee],
    };
  }),
  on(updateEmploy, (state, action) => {
    const updatePosts = state.employ.map((employee) => {
      return action.employ.employeeID === employee.employeeID
        ? action.employ
        : employee;
    });
    return {
      ...state,
      employ: updatePosts,
    };
  }),
  on(deleteEmploy, (state, action) => {
    const updatePosts = state.employ.filter((post) => {
      return post.id !== action.id;
    });
    console.log(state);
    return {
      ...state,
      employ: updatePosts,
    };
  })
);

export function employeeReducer(state: any, action: any) {
  return _employeeReducer(state, action);
}
