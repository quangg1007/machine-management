import { Component } from '@angular/core';

@Component({
  selector: 'app-edit-employ-management-form-view',
  templateUrl: './edit-employ-management-form.view.html',
  styleUrls: ['./edit-employ-management-form.view.scss'],
})
export class EditEmployManagementFormView {}
