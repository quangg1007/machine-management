import { Component } from '@angular/core';

@Component({
  selector: 'app-add-employ-management-form-view',
  templateUrl: './add-employ-management-form.view.html',
  styleUrls: ['./add-employ-management-form.view.scss'],
})
export class AddEmployManagementFormView {}
