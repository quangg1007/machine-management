import { Component } from '@angular/core';

@Component({
  selector: 'app-employ-management-list-view',
  templateUrl: './employ-management-list.view.html',
  styleUrls: ['./employ-management-list.view.scss'],
})
export class EmployManagementListView {}
