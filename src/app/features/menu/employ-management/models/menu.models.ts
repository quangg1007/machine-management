export interface assignInfo {
  name: string;
  image?: string;
  employeeID: number;
}
export interface employType {
  id?: number;
  assign: assignInfo;
  employeeID: number;
  cccd: string;
  accommodation: string;
  phoneNumber: string;
  email: string;
  position: string;
  experience: string;
  button: string;
}

export interface positionType {
  value: string;
  viewValue: string;
}
