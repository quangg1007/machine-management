import { LocationStrategy } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { employType, positionType } from '../../models/menu.models';
import { Store } from '@ngrx/store';
import { updateEmploy } from '@app/features/menu/employ-management/state/employ-management.actions';
import { AppState } from '@app/features/menu/store/app.state';
import { POSTION_VALUE_DATA } from '../../state/employ-managment.state';

@Component({
  selector: 'app-edit-employ-management-form-container',
  templateUrl: './edit-employ-management-form.container.html',
  styleUrls: ['./edit-employ-management-form.container.scss'],
})
export class EditEmployManagementFormContainer {
  userForm: FormGroup;
  positionValue: positionType[] = POSTION_VALUE_DATA;

  constructor(
    private location: LocationStrategy,
    private fb: FormBuilder,
    private store: Store<AppState>
  ) {
    const userInfo = this.location.getState() as employType;

    this.userForm = this.fb.group({
      id: userInfo.id,
      name: [
        userInfo?.assign?.name,
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(30),
          Validators.pattern('^[a-zA-Z]+$'),
        ],
      ],
      employeeID: [userInfo?.employeeID, [Validators.required]],
      cccd: [userInfo?.cccd, [Validators.required]],
      phoneNumber: [userInfo?.phoneNumber, [Validators.required]],
      accommodation: [userInfo?.accommodation, [Validators.required]],
      email: [userInfo?.email, [Validators.required, Validators.email]],
      position: [userInfo?.position, [Validators.required]],
      experience: [userInfo?.experience, [Validators.required]],
    });
  }

  back() {
    this.location.back();
  }

  onFormSubmit(): void {
    console.log(this.userForm.value);

    const employInfo: employType = {
      id: this.userForm.value.id,
      assign: {
        name: this.userForm.value.name,
        image: 'assets/image/Phạm Minh Quang.jpg',
        employeeID: this.userForm.value.employeeID,
      },
      employeeID: this.userForm.value.employeeID,
      cccd: this.userForm.value.cccd,
      phoneNumber: this.userForm.value.phoneNumber,
      accommodation: this.userForm.value.accommodation,
      email: this.userForm.value.email,
      position: this.userForm.value.position,
      experience: this.userForm.value.experience,
      button: '',
    };

    this.store.dispatch(updateEmploy({ employ: employInfo }));
    this.location.back();
  }

  onFormClear(): void {
    this.userForm.reset();
  }

  showNameError(prop: string) {
    const nameForm = this.userForm.get(prop);
    if (nameForm?.touched && !nameForm.valid) {
      if (nameForm?.errors?.['required']) {
        return `${this.returnMessage(prop)} bắt buộc `;
      }
      if (nameForm?.errors?.['minlength']) {
        return `${this.returnMessage(prop)} nên có tối thiểu từ 10 kí tự`;
      }
    }
    return;
  }

  returnMessage(message: string): string {
    switch (message) {
      case 'name': {
        return 'Tên';
        break;
      }
      case 'employeeID': {
        return 'Mã nhân viên';
        break;
      }
      case 'phoneNumber': {
        return 'Số điện thoại';
        break;
      }
      case 'cccd': {
        return 'CCCD';
        break;
      }
      case 'email': {
        return 'Email';
        break;
      }
      case 'accommodation': {
        return 'Nơi ở';
        break;
      }
      case 'position': {
        return 'Vị trí';
        break;
      }
      case 'experience': {
        return 'Kinh nghiệm';
        break;
      }
      default: {
        return '';
      }
    }
  }
}
