import { Component, Input } from '@angular/core';
import { employType } from '../../models/menu.models';
import { Router } from '@angular/router';
import { EMPLOYEE_DATA } from '../../state/employ-managment.state';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { getEmployee } from '../../state/employ-management.selector';
import { deleteEmploy } from '../../state/employ-management.actions';

@Component({
  selector: 'app-employ-management-list-container',
  templateUrl: './employ-management-list.container.html',
  styleUrls: ['./employ-management-list.container.scss'],
})
export class EmployManagementListContainer {
  @Input() dataSource: employType[];
  posts: Observable<employType[]>;

  constructor(private _router: Router, private store: Store) {
    this.dataSource = EMPLOYEE_DATA.employ;
    this.posts = this.store.select(getEmployee);
    this.posts.subscribe((val) => {
      this.dataSource = val;
    });
  }

  userInfo(index: number): employType | any {
    return {
      id: this.dataSource[index].id,
      assign: this.dataSource[index].assign,
      employeeID: this.dataSource[index].employeeID,
      cccd: this.dataSource[index].cccd,
      accommodation: this.dataSource[index].accommodation,
      phoneNumber: this.dataSource[index].phoneNumber,
      email: this.dataSource[index].email,
      position: this.dataSource[index].position,
      experience: this.dataSource[index].experience,
      button: this.dataSource[index].button,
    };
  }

  routeToEdit(id: number): void {
    console.log(this.userInfo(id));
    this._router.navigateByUrl('/home/danh-sach-quan-ly-nhan-vien/chinh-sua', {
      state: this.userInfo(id),
    });
  }

  deleteEmployee(id: number) {
    if (confirm('Bạn có chắc xóa thông tin!')) {
      console.log('delete the post');
    }

    this.store.dispatch(deleteEmploy({ id: id }));
  }

  displayedColumns: string[] = [
    'assign',
    'employeeID',
    'cccd',
    'accommodation',
    'email',
    'position',
    'experience',
    'button',
  ];
}
