import { assignInfo } from '../../employ-management/models/menu.models';

export interface FixInformationType {
  id?: number;
  assignID: number;
  productName: string;
  quantity: number;
  fixingDate: string;
  machineStatus: string;
  button: string;
}

export interface FixInformationDisplayType {
  id?: number;
  assignID: assignInfo;
  productName: string;
  quantity: number;
  fixingDate: string;
  machineStatus: string;
  button: string;
}
