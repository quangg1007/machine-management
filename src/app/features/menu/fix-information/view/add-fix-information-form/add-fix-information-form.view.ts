import { Component } from '@angular/core';

@Component({
  selector: 'app-add-fix-information-form-view',
  templateUrl: './add-fix-information-form.view.html',
  styleUrls: ['./add-fix-information-form.view.scss'],
})
export class AddFixInformationFormView {}
