import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {
  AddFixInformationFormView,
  EditFixInformationFormView,
  FixInformationListView,
} from './view';

const routes: Routes = [
  {
    path: '',
    component: FixInformationListView,
  },
  {
    path: 'chinh-sua',
    component: EditFixInformationFormView,
  },
  {
    path: 'them-moi',
    component: AddFixInformationFormView,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FixInformationRoutingModule {}
