import { LocationStrategy } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/features/menu/store/app.state';
import { Store } from '@ngrx/store';
import { FixInformationType } from '../../models/menu.models';
import { addFixInfo } from '../../state/fix-information.actions';
import { assignInfo } from '@app/features/menu/employ-management/models/menu.models';
import { getEmployee } from '@app/features/menu/employ-management/state/employ-management.selector';

@Component({
  selector: 'app-add-fix-information-form-container',
  templateUrl: './add-fix-information-form.container.html',
  styleUrls: ['./add-fix-information-form.container.scss'],
})
export class AddFixInformationFormContainer {
  fixInfoForm: FormGroup;
  assignName: assignInfo[] = [];
  constructor(
    private location: LocationStrategy,
    private fb: FormBuilder,
    private store: Store<AppState>
  ) {
    this.fixInfoForm = this.fb.group({
      productName: ['', [Validators.required]],
      assignID: ['', [Validators.required]],
      quantity: ['', [Validators.required]],
      fixingDate: ['', [Validators.required]],
      machineStatus: ['', [Validators.required]],
    });
    this.store.select(getEmployee).subscribe((value) => {
      value.map((val, index) => {
        this.assignName[index] = val.assign;
      });
    });
  }

  back() {
    this.location.back();
  }

  onFormSubmit(): void {
    console.log(this.fixInfoForm.value);

    const fixInfo: FixInformationType = {
      assignID: this.fixInfoForm.value.assignID,
      productName: this.fixInfoForm.value.productName,
      quantity: this.fixInfoForm.value.quantity,
      fixingDate: this.fixInfoForm.value.fixingDate,
      machineStatus: this.fixInfoForm.value.machineStatus,
      button: this.fixInfoForm.value.button,
    };

    this.store.dispatch(addFixInfo({ info: fixInfo }));
    this.location.back();
  }

  onFormClear(): void {
    this.fixInfoForm.reset();
  }

  showNameError(prop: string) {
    const nameForm = this.fixInfoForm.get(prop);
    if (nameForm?.touched && !nameForm.valid) {
      if (nameForm?.errors?.['required']) {
        return `${this.returnMessage(prop)} bắt buộc `;
      }
      if (nameForm?.errors?.['minlength']) {
        return `${this.returnMessage(prop)} nên có tối thiểu từ 10 kí tự`;
      }
    }
    return;
  }

  returnMessage(message: string): string {
    switch (message) {
      case 'productName': {
        return 'Tên máy';
        break;
      }
      case 'quantity': {
        return 'Số lượng máy';
        break;
      }
      case 'fixingDate': {
        return 'Thời gian sữa chữa';
        break;
      }
      case 'machineStatus': {
        return 'Tình trạng máy';
        break;
      }
      default: {
        return '';
      }
    }
  }
}
