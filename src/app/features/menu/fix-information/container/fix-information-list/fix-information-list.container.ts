import { Component } from '@angular/core';
import {
  FixInformationDisplayType,
  FixInformationType,
} from '../../models/menu.models';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { getInfos } from '../../state/fix-information.selector';
import { deleteFixInfo } from '../../state/fix-information.actions';
import { getEmployee } from '@app/features/menu/employ-management/state/employ-management.selector';
import { employType } from '@app/features/menu/employ-management/models/menu.models';

@Component({
  selector: 'app-fix-information-list-container',
  templateUrl: './fix-information-list.container.html',
  styleUrls: ['./fix-information-list.container.scss'],
})
export class FixInformationListContainer {
  dataSource: FixInformationDisplayType[] = [];
  fixInfoMachine: Observable<FixInformationType[]>;
  assignInfo: Observable<employType[]>;

  constructor(private _router: Router, private store: Store) {
    this.assignInfo = this.store.select(getEmployee);
    this.fixInfoMachine = this.store.select(getInfos);
    this.fixInfoMachine.subscribe((info) => {
      this.dataSource = [];
      info.map((info) => {
        this.assignInfo.subscribe((assign) => {
          assign.map((id) => {
            if (id.assign.employeeID === info.assignID) {
              this.dataSource = [
                ...this.dataSource,
                {
                  id: info.id,
                  assignID: id.assign,
                  productName: info.productName,
                  quantity: info.quantity,
                  fixingDate: info.fixingDate,
                  machineStatus: info.machineStatus,
                  button: '',
                },
              ];
            }
          });
        });
      });
    });
  }

  fixInfo(index: number): FixInformationDisplayType | any {
    return {
      id: this.dataSource[index].id,
      assignID: this.dataSource[index].assignID,
      productName: this.dataSource[index].productName,
      quantity: this.dataSource[index].quantity,
      fixingDate: this.dataSource[index].fixingDate,
      machineStatus: this.dataSource[index].machineStatus,
      button: this.dataSource[index].button,
    };
  }

  routeToEdit(index: number): void {
    // console.log(this.fixInfo(id));
    this._router.navigateByUrl('/home/danh-sach-thong-tin-sua-chua/chinh-sua', {
      state: this.fixInfo(index),
    });
  }

  deleteInfo(id: number) {
    if (confirm('Bạn có chắc xóa thông tin!')) {
      console.log('delete the post');
    }

    this.store.dispatch(deleteFixInfo({ id: id }));
  }

  displayedColumns: string[] = [
    'productName',
    'assign',
    'quantity',
    'FixingDate',
    'button',
  ];
}
