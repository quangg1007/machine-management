import { LocationStrategy } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/features/menu/store/app.state';
import { Store } from '@ngrx/store';
import {
  FixInformationDisplayType,
  FixInformationType,
} from '../../models/menu.models';
import { updateFixInfo } from '../../state/fix-information.actions';
import { assignInfo } from '@app/features/menu/employ-management/models/menu.models';
import { getEmployee } from '@app/features/menu/employ-management/state/employ-management.selector';

@Component({
  selector: 'app-edit-fix-information-form-container',
  templateUrl: './edit-fix-information-form.container.html',
  styleUrls: ['./edit-fix-information-form.container.scss'],
})
export class EditFixInformationFormContainer {
  fixingForm: FormGroup;
  assignName: assignInfo[] = [];
  selected: number;

  constructor(
    private location: LocationStrategy,
    private fb: FormBuilder,
    private store: Store<AppState>
  ) {
    const fixingInfo = this.location.getState() as FixInformationDisplayType;
    this.store.select(getEmployee).subscribe((value) => {
      value.map((val, index) => {
        this.assignName[index] = val.assign;
      });
    });
    console.log(fixingInfo);
    this.selected = fixingInfo.assignID.employeeID;
    this.fixingForm = this.fb.group({
      id: fixingInfo.id,
      assignID: [fixingInfo.assignID.employeeID, [Validators.required]],
      productName: [fixingInfo.productName, [Validators.required]],
      quantity: [fixingInfo.quantity, [Validators.required]],
      fixingDate: [fixingInfo.fixingDate, [Validators.required]],
      machineStatus: [fixingInfo.machineStatus, [Validators.required]],
      button: [fixingInfo.button, [Validators.required]],
    });
  }

  back() {
    this.location.back();
  }

  onFormSubmit(): void {
    const fixingInfo: FixInformationType = {
      id: this.fixingForm.value.id,
      assignID: this.fixingForm.value.assignID,
      productName: this.fixingForm.value.productName,
      quantity: this.fixingForm.value.quantity,
      fixingDate: this.fixingForm.value.fixingDate,
      machineStatus: this.fixingForm.value.machineStatus,
      button: '',
    };
    console.log(fixingInfo);
    this.store.dispatch(updateFixInfo({ info: fixingInfo }));
    this.location.back();
  }

  onFormClear(): void {
    this.fixingForm.reset();
  }

  showNameError(prop: string) {
    const nameForm = this.fixingForm.get(prop);
    if (nameForm?.touched && !nameForm.valid) {
      if (nameForm?.errors?.['required']) {
        return `${this.returnMessage(prop)} bắt buộc `;
      }
      if (nameForm?.errors?.['minlength']) {
        return `${this.returnMessage(prop)} nên có tối thiểu từ 10 kí tự`;
      }
    }
    return;
  }

  returnMessage(message: string): string {
    switch (message) {
      case 'productName': {
        return 'Tên máy';
        break;
      }
      case 'quantity': {
        return 'Số lượng máy';
        break;
      }
      case 'fixingDate': {
        return 'Thời gian sữa chữa';
        break;
      }
      case 'machineStatus': {
        return 'Tình trạng máy';
        break;
      }
      default: {
        return '';
      }
    }
  }
}
