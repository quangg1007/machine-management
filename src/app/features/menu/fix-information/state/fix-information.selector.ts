import { createFeatureSelector, createSelector } from '@ngrx/store';
import { FixInformationState } from './fix-information.state';

const getFixInfoState = createFeatureSelector<FixInformationState>('info');

export const getInfos = createSelector(getFixInfoState, (state) => {
  return state.info;
});
