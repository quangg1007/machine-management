import { createReducer, on } from '@ngrx/store';
import { FIX_INFO_DATA } from './fix-information.state';
import {
  addFixInfo,
  deleteFixInfo,
  updateFixInfo,
} from './fix-information.actions';

const _fixInfoReducer = createReducer(
  FIX_INFO_DATA,
  on(addFixInfo, (state, action) => {
    let fixInfo = { ...action.info };
    let lastIndex: number | any;
    lastIndex = state.info[state.info.length - 1].id;
    fixInfo.id = lastIndex + 1;
    return {
      ...state,
      info: [...state.info, fixInfo],
    };
  }),
  on(updateFixInfo, (state, action) => {
    const updateFixInfo = state.info.map((fixInfo) => {
      return action.info.id === fixInfo.id ? action.info : fixInfo;
    });
    return {
      ...state,
      info: updateFixInfo,
    };
  }),
  on(deleteFixInfo, (state, action) => {
    const updateFixInfos = state.info.filter((post) => {
      return post.id !== action.id;
    });
    return {
      ...state,
      info: updateFixInfos,
    };
  })
);

export function fixInfoReducer(state: any, action: any) {
  return _fixInfoReducer(state, action);
}
