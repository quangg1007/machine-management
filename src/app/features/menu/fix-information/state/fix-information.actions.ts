import { createAction, props } from '@ngrx/store';
import { FixInformationType } from '../models/menu.models';

export const ADD_FIX_INFO_ACTION = '[fixing page] add info';
export const UPDATE_FIX_INFO_ACTION = '[fixing page] update info';
export const DELETE_FIX_INFO_ACTION = '[fixing page] delete info';

export const addFixInfo = createAction(
  ADD_FIX_INFO_ACTION,
  props<{ info: FixInformationType }>()
);

export const updateFixInfo = createAction(
  UPDATE_FIX_INFO_ACTION,
  props<{ info: FixInformationType }>()
);

export const deleteFixInfo = createAction(
  DELETE_FIX_INFO_ACTION,
  props<{ id: number }>()
);
