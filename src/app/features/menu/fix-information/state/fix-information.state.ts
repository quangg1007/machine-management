import { FixInformationType } from '../models/menu.models';

export interface FixInformationState {
  info: FixInformationType[];
}

export const FIX_INFO_DATA: FixInformationState = {
  info: [
    {
      id: 0,
      productName: 'Redesign Brief 2019.pdf',
      assignID: 475847,
      quantity: 2,
      fixingDate: '08-03-2023',
      machineStatus: 'Máy đang được đi bảo hành tại công ty xyz',
      button: '',
    },
    {
      id: 1,
      productName: 'Máy xét nghiệm ung thư',
      assignID: 20203,
      quantity: 6,
      fixingDate: '08-03-2023',
      machineStatus: 'Máy đang được đi bảo hành tại công ty xyz',
      button: '',
    },
    {
      id: 2,
      productName: 'Máy xét nghiệm vom hong',
      assignID: 13579,
      quantity: 5,
      fixingDate: '08-03-2023',
      machineStatus: 'Máy đang được đi bảo hành tại công ty xyz',
      button: '',
    },
    {
      id: 3,
      productName: 'Máy noi soi',
      assignID: 8749857,
      quantity: 1,
      fixingDate: '08-03-2023',
      machineStatus: 'Máy đang được đi bảo hành tại công ty xyz',
      button: '',
    },
    {
      id: 4,
      productName: 'Máy x-quang',
      assignID: 20203,
      quantity: 5,
      fixingDate: '08-03-2023',
      machineStatus: 'Máy đang được đi bảo hành tại công ty xyz',
      button: '',
    },
  ],
};
