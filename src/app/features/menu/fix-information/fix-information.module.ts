import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { PaginatorComponent } from '@app/shared/component';
import { FixInformationRoutingModule } from './fix-information-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import {
  FixInformationListContainer,
  AddFixInformationFormContainer,
  EditFixInformationFormContainer,
} from './container';
import {
  AddFixInformationFormView,
  EditFixInformationFormView,
  FixInformationListView,
} from './view';

const containers = [
  FixInformationListContainer,
  AddFixInformationFormContainer,
  EditFixInformationFormContainer,
];
const views = [
  AddFixInformationFormView,
  EditFixInformationFormView,
  FixInformationListView,
];
const components: any[] = [];

@NgModule({
  declarations: [...containers, ...views, ...components],
  imports: [
    CommonModule,
    FixInformationRoutingModule,
    ReactiveFormsModule,
    PaginatorComponent,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
  ],
})
export class EmployManagementModule {}
