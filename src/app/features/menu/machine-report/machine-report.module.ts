import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { PaginatorComponent } from '@app/shared/component';
import { MachineReportRoutingModule } from './machine-report-routing.module';
import {
  AddMachineReportFormView,
  MachineReportListView,
  EditMachineReportFormView,
} from './view';
import {
  AddMachineReportFormContainer,
  MachineReportListContainer,
  EditMachineReportFormContainer,
} from './container';
import { ReactiveFormsModule } from '@angular/forms';

const containers = [
  MachineReportListContainer,
  AddMachineReportFormContainer,
  EditMachineReportFormContainer,
];
const views = [
  AddMachineReportFormView,
  MachineReportListView,
  EditMachineReportFormView,
];
const components: any[] = [];

@NgModule({
  declarations: [...containers, ...views, ...components],
  imports: [
    CommonModule,
    MachineReportRoutingModule,
    PaginatorComponent,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
  ],
})
export class MachineReportModule {}
