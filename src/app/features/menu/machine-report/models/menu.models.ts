import { assignInfo } from '../../employ-management/models/menu.models';

export interface machineInfoType {
  id?: number;
  productName: string;
  assign: number;
  productCode: string;
  serialCode: string;
  shape: string;
  country: string;
  dateOfManufature: string;
  fixingHistory: string;
  supplier: string;
  producer: string;
  button: string;
}

export interface machineInfoDisplayType {
  id?: number;
  productName: string;
  assign: assignInfo;
  productCode: string;
  serialCode: string;
  shape: string;
  country: string;
  dateOfManufature: string;
  fixingHistory: string;
  supplier: string;
  producer: string;
  button: string;
}
