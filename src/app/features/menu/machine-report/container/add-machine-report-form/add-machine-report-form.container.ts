import { LocationStrategy } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { assignInfo } from '@app/features/menu/employ-management/models/menu.models';
import { AppState } from '@app/features/menu/store/app.state';
import { Store } from '@ngrx/store';
import { getMachine } from '../../state/machine-report.selector';
import { getEmployee } from '@app/features/menu/employ-management/state/employ-management.selector';
import { machineInfoType } from '../../models/menu.models';
import { addMachine } from '../../state/machine-report.actions';

@Component({
  selector: 'app-add-machine-report-form-container',
  templateUrl: './add-machine-report-form.container.html',
  styleUrls: ['./add-machine-report-form.container.scss'],
})
export class AddMachineReportFormContainer {
  machineInfoForm: FormGroup;
  assignName: assignInfo[] = [];

  constructor(
    private location: LocationStrategy,
    private fb: FormBuilder,
    private store: Store<AppState>
  ) {
    this.machineInfoForm = this.fb.group({
      productName: ['', [Validators.required]],
      assign: ['', [Validators.required]],
      productCode: ['', [Validators.required]],
      serialCode: ['', [Validators.required]],
      shape: ['', [Validators.required]],
      country: ['', [Validators.required]],
      dateOfManufature: ['', [Validators.required]],
      fixingHistory: ['', [Validators.required]],
      supplier: ['', [Validators.required]],
      producer: ['', [Validators.required]],
    });
    this.store.select(getEmployee).subscribe((value) => {
      value.map((val, index) => {
        this.assignName[index] = val.assign;
      });
    });
  }

  back() {
    this.location.back();
  }
  onFormSubmit(): void {
    const machineInfo: machineInfoType = {
      productName: this.machineInfoForm.value.productName,
      assign: this.machineInfoForm.value.assign,
      productCode: this.machineInfoForm.value.productCode,
      serialCode: this.machineInfoForm.value.serialCode,
      shape: this.machineInfoForm.value.shape,
      country: this.machineInfoForm.value.country,
      dateOfManufature: this.machineInfoForm.value.dateOfManufature,
      fixingHistory: this.machineInfoForm.value.fixingHistory,
      supplier: this.machineInfoForm.value.supplier,
      producer: this.machineInfoForm.value.producer,
      button: this.machineInfoForm.value.button,
    };

    console.log(this.machineInfoForm.value);
    this.store.dispatch(addMachine({ machine: machineInfo }));
    this.location.back();
  }
  onFormClear(): void {
    this.machineInfoForm.reset();
  }

  showNameError(prop: string) {
    const nameForm = this.machineInfoForm.get(prop);
    if (nameForm?.touched && !nameForm.valid) {
      if (nameForm?.errors?.['required']) {
        return `${this.returnMessage(prop)} bắt buộc `;
      }
      if (nameForm?.errors?.['minlength']) {
        return `${this.returnMessage(prop)} nên có tối thiểu từ 10 kí tự`;
      }
    }
    return;
  }

  returnMessage(message: string): string {
    switch (message) {
      case 'productName': {
        return 'Tên thiết bị';
        break;
      }
      case 'productCode': {
        return 'Mã máy';
        break;
      }
      case 'serialCode': {
        return 'Mã Serial';
        break;
      }
      case 'shape': {
        return 'Kiểu dáng';
        break;
      }
      case 'country': {
        return 'Nước';
        break;
      }
      case 'dateOfManufature': {
        return 'Ngày sản xuất';
        break;
      }
      case 'fixingHistory': {
        return 'Lịch sử sửa chữa';
        break;
      }
      case 'supplier': {
        return 'Nhà cung cấp';
        break;
      }
      case 'producer': {
        return 'Nhà sản xuất';
        break;
      }
      default: {
        return '';
      }
    }
  }
}
