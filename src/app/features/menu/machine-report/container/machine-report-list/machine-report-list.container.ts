import { Component } from '@angular/core';
import {
  machineInfoDisplayType,
  machineInfoType,
} from '../../models/menu.models';
import { Observable } from 'rxjs';
import { employType } from '@app/features/menu/employ-management/models/menu.models';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { getMachine } from '../../state/machine-report.selector';
import { getEmployee } from '@app/features/menu/employ-management/state/employ-management.selector';
import { deleteMachine } from '../../state/machine-report.actions';

@Component({
  selector: 'app-machine-report-list-container',
  templateUrl: './machine-report-list.container.html',
  styleUrls: ['./machine-report-list.container.scss'],
})
export class MachineReportListContainer {
  dataSource: machineInfoDisplayType[] = [];
  machineReport: Observable<machineInfoType[]>;
  assignInfo: Observable<employType[]>;

  constructor(private _router: Router, private store: Store) {
    this.assignInfo = this.store.select(getEmployee);
    this.machineReport = this.store.select(getMachine);
    this.machineReport.subscribe((info) => {
      this.dataSource = [];
      info.map((info) => {
        this.assignInfo.subscribe((assign) => {
          assign.map((id) => {
            if (id.assign.employeeID === info.assign) {
              this.dataSource = [
                ...this.dataSource,
                {
                  id: info.id,
                  productName: info.productName,
                  productCode: info.productCode,
                  serialCode: info.serialCode,
                  shape: info.shape,
                  country: info.country,
                  assign: id.assign,
                  dateOfManufature: info.dateOfManufature,
                  fixingHistory: info.fixingHistory,
                  supplier: info.supplier,
                  producer: info.producer,
                  button: '',
                },
              ];
            }
          });
        });
      });
    });
  }

  machineInfo(index: number): machineInfoDisplayType | any {
    return {
      id: this.dataSource[index].id,
      productName: this.dataSource[index].productName,
      productCode: this.dataSource[index].productCode,
      serialCode: this.dataSource[index].serialCode,
      shape: this.dataSource[index].shape,
      country: this.dataSource[index].country,
      assign: this.dataSource[index].assign,
      dateOfManufature: this.dataSource[index].dateOfManufature,
      fixingHistory: this.dataSource[index].fixingHistory,
      supplier: this.dataSource[index].supplier,
      producer: this.dataSource[index].producer,
      button: this.dataSource[index].button,
    };
  }

  routeToEdit(index: number): void {
    console.log(this.machineInfo(index));
    this._router.navigateByUrl('/home/ho-so-may-moc/chinh-sua', {
      state: this.machineInfo(index),
    });
  }

  deleteMachine(id: number) {
    if (confirm('Bạn có chắc xóa thông tin!')) {
      console.log('delete the post');
    }

    this.store.dispatch(deleteMachine({ id: id }));
  }

  displayedColumns: string[] = [
    'productName',
    'productCode',
    'serialCode',
    'shape',
    'country',
    'assign',
    'dateOfManufature',
    'fixingHistory',
    'button',
  ];
}
