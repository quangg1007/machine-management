import { LocationStrategy } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { assignInfo } from '@app/features/menu/employ-management/models/menu.models';
import { AppState } from '@app/features/menu/store/app.state';
import { Store } from '@ngrx/store';
import {
  machineInfoDisplayType,
  machineInfoType,
} from '../../models/menu.models';
import { getEmployee } from '@app/features/menu/employ-management/state/employ-management.selector';
import { updateMachine } from '../../state/machine-report.actions';

@Component({
  selector: 'app-edit-machine-report-form-container',
  templateUrl: './edit-machine-report-form.container.html',
  styleUrls: ['./edit-machine-report-form.container.scss'],
})
export class EditMachineReportFormContainer {
  machineInfoForm: FormGroup;
  assignName: assignInfo[] = [];
  selected: number;

  constructor(
    private location: LocationStrategy,
    private fb: FormBuilder,
    private store: Store<AppState>
  ) {
    const machineInfo = this.location.getState() as machineInfoDisplayType;
    this.store.select(getEmployee).subscribe((value) => {
      value.map((val, index) => {
        this.assignName[index] = val.assign;
      });
    });
    console.log(machineInfo);
    this.selected = machineInfo.assign.employeeID;
    this.machineInfoForm = this.fb.group({
      id: machineInfo.id,
      productName: [machineInfo.productName, [Validators.required]],
      assign: [machineInfo.assign.employeeID, [Validators.required]],
      productCode: [machineInfo.productCode, [Validators.required]],
      serialCode: [machineInfo.serialCode, [Validators.required]],
      shape: [machineInfo.shape, [Validators.required]],
      country: [machineInfo.country, [Validators.required]],
      dateOfManufature: [machineInfo.dateOfManufature, [Validators.required]],
      fixingHistory: [machineInfo.fixingHistory, [Validators.required]],
      supplier: [machineInfo.supplier, [Validators.required]],
      producer: [machineInfo.producer, [Validators.required]],
    });

    console.log(this.machineInfoForm);
  }

  back() {
    this.location.back();
  }

  onFormSubmit(): void {
    const machineInfo: machineInfoType = {
      id: this.machineInfoForm.value.id,
      productName: this.machineInfoForm.value.productName,
      assign: this.machineInfoForm.value.assign,
      productCode: this.machineInfoForm.value.productCode,
      serialCode: this.machineInfoForm.value.serialCode,
      shape: this.machineInfoForm.value.shape,
      country: this.machineInfoForm.value.country,
      dateOfManufature: this.machineInfoForm.value.dateOfManufature,
      fixingHistory: this.machineInfoForm.value.fixingHistory,
      supplier: this.machineInfoForm.value.supplier,
      producer: this.machineInfoForm.value.producer,
      button: '',
    };
    this.store.dispatch(updateMachine({ machine: machineInfo }));
    this.location.back();
  }

  onFormClear(): void {
    this.machineInfoForm.reset();
  }

  showNameError(prop: string) {
    const nameForm = this.machineInfoForm.get(prop);
    if (nameForm?.touched && !nameForm.valid) {
      if (nameForm?.errors?.['required']) {
        return `${this.returnMessage(prop)} bắt buộc `;
      }
      if (nameForm?.errors?.['minlength']) {
        return `${this.returnMessage(prop)} nên có tối thiểu từ 10 kí tự`;
      }
    }
    return;
  }

  returnMessage(message: string): string {
    switch (message) {
      case 'productName': {
        return 'Tên thiết bị';
        break;
      }
      case 'productCode': {
        return 'Mã máy';
        break;
      }
      case 'serialCode': {
        return 'Mã Serial';
        break;
      }
      case 'shape': {
        return 'Kiểu dáng';
        break;
      }
      case 'country': {
        return 'Nước';
        break;
      }
      case 'dateOfManufature': {
        return 'Ngày sản xuất';
        break;
      }
      case 'fixingHistory': {
        return 'Lịch sử sửa chữa';
        break;
      }
      case 'supplier': {
        return 'Nhà cung cấp';
        break;
      }
      case 'producer': {
        return 'Nhà sản xuất';
        break;
      }
      default: {
        return '';
      }
    }
  }
}
