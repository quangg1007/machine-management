import { createFeatureSelector, createSelector } from '@ngrx/store';
import { machineInfoState } from './machine-report.state';

const getMachineState = createFeatureSelector<machineInfoState>('machine');

export const getMachine = createSelector(getMachineState, (state) => {
  return state.machine;
});
