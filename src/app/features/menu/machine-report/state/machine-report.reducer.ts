import { createReducer, on } from '@ngrx/store';
import { MACHINE_REPORT_DATA } from './machine-report.state';
import {
  addMachine,
  deleteMachine,
  updateMachine,
} from './machine-report.actions';

const _machineReducer = createReducer(
  MACHINE_REPORT_DATA,
  on(addMachine, (state, action) => {
    let machineInfo = { ...action.machine };
    let lastIndex: number | any;
    lastIndex = state.machine[state.machine.length - 1].id;
    machineInfo.id = lastIndex + 1;
    return {
      ...state,
      machine: [...state.machine, machineInfo],
    };
  }),
  on(updateMachine, (state, action) => {
    const updateMachine = state.machine.map((machineInfo) => {
      return action.machine.id === machineInfo.id
        ? action.machine
        : machineInfo;
    });
    return {
      ...state,
      machine: updateMachine,
    };
  }),
  on(deleteMachine, (state, action) => {
    const updateMachine = state.machine.filter((post) => {
      return post.id !== action.id;
    });
    console.log(state);
    return {
      ...state,
      machine: updateMachine,
    };
  })
);

export function machineReportReducer(state: any, action: any) {
  return _machineReducer(state, action);
}
