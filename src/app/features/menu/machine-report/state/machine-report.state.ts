import { machineInfoType } from '../models/menu.models';

export interface machineInfoState {
  machine: machineInfoType[];
}

export const MACHINE_REPORT_DATA: machineInfoState = {
  machine: [
    {
      id: 0,
      productName: 'Redesign Brief 2019.pdf',
      productCode: 'abcxyz',
      serialCode: 'abcxyz',
      shape: 'abcxyz',
      country: 'Germany',
      assign: 13579,
      dateOfManufature: '08 Jan 2019',
      fixingHistory: '08 Jan 2019',
      supplier: 'CTNNHH 1 thành viên Roce',
      producer: 'CTNNHH 1 thành viên Roce',
      button: '',
    },
    {
      id: 1,
      productName: 'Redesign Brief 2019.pdf',
      productCode: 'html',
      serialCode: 'css',
      shape: 'vuông',
      country: 'Germany',
      assign: 13579,
      dateOfManufature: '08 Jan 2019',
      fixingHistory: '08 Jan 2019',
      supplier: 'CTNNHH 1 thành viên Roce',
      producer: 'CTNNHH 1 thành viên Roce',
      button: '',
    },
  ],
};
