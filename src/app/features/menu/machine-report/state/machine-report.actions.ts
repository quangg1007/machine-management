import { createAction, props } from '@ngrx/store';
import { machineInfoType } from '../models/menu.models';

export const ADD_MACHINE_REPORT_ACTION = '[machine page] add machine';
export const UPDATE_MACHINE_REPORT_ACTION = '[machine page] update machine';
export const DELETE_MACHINE_REPORT_ACTION = '[machine page] delete machine';

export const addMachine = createAction(
  ADD_MACHINE_REPORT_ACTION,
  props<{ machine: machineInfoType }>()
);

export const updateMachine = createAction(
  UPDATE_MACHINE_REPORT_ACTION,
  props<{ machine: machineInfoType }>()
);

export const deleteMachine = createAction(
  DELETE_MACHINE_REPORT_ACTION,
  props<{ id: number }>()
);
