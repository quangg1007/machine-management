import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {
  AddMachineReportFormView,
  EditMachineReportFormView,
  MachineReportListView,
} from './view';

const routes: Routes = [
  {
    path: '',
    component: MachineReportListView,
  },
  {
    path: 'them-moi',
    component: AddMachineReportFormView,
  },
  {
    path: 'chinh-sua',
    component: EditMachineReportFormView,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MachineReportRoutingModule {}
