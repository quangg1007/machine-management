import { Component } from '@angular/core';

@Component({
  selector: 'app-add-machine-report-form-view',
  templateUrl: './add-machine-report-form.view.html',
  styleUrls: ['./add-machine-report-form.view.scss'],
})
export class AddMachineReportFormView {}
