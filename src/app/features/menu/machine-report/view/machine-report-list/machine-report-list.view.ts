import { Component } from '@angular/core';

@Component({
  selector: 'app-machine-report-list-view',
  templateUrl: './machine-report-list.view.html',
  styleUrls: ['./machine-report-list.view.scss'],
})
export class MachineReportListView {}
