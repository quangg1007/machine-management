import { Component } from '@angular/core';

@Component({
  selector: 'app-edit-machine-report-form-view',
  templateUrl: './edit-machine-report-form.view.html',
  styleUrls: ['./edit-machine-report-form.view.scss'],
})
export class EditMachineReportFormView {}
