export * from './statistical-report/statistical-report.view';
export * from './add-statistical-report-form/add-statistical-report-form.view';
export * from './edit-statistical-report-form/edit-statistical-report-form.view';
