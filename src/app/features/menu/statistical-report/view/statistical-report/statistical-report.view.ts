import { Component } from '@angular/core';

@Component({
  selector: 'app-statistical-report-view',
  templateUrl: './statistical-report.view.html',
  styleUrls: ['./statistical-report.view.scss'],
})
export class StatisticalReportView {}
