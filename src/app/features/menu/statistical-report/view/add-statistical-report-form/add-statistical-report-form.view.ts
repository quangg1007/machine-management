import { Component } from '@angular/core';

@Component({
  selector: 'app-add-statistical-report-form-view',
  templateUrl: './add-statistical-report-form.view.html',
  styleUrls: ['./add-statistical-report-form.view.scss'],
})
export class AddStatisticalReportFormView {}
