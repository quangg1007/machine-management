import { Component } from '@angular/core';

@Component({
  selector: 'app-edit-statistical-report-form-view',
  templateUrl: './edit-statistical-report-form.view.html',
  styleUrls: ['./edit-statistical-report-form.view.scss'],
})
export class EditStatisticalReportFormView {}
