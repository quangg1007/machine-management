import { createFeatureSelector, createSelector } from '@ngrx/store';
import { staticReportState } from './statistical-report.state';

const getStatisticalReportState =
  createFeatureSelector<staticReportState>('report');

export const getStatisticalReport = createSelector(
  getStatisticalReportState,
  (state) => {
    return state.report;
  }
);
