import { createReducer, on } from '@ngrx/store';
import { STATISTICAL_REPORT_DATA } from './statistical-report.state';
import {
  addStatisticalReport,
  deleteStatisticalReport,
  updateStatisticalReport,
} from './statistical-report.action';

const _statisticalInfoReducer = createReducer(
  STATISTICAL_REPORT_DATA,
  on(addStatisticalReport, (state, action) => {
    let statisticalInfo = { ...action.report };
    let lastIndex: number | any;
    lastIndex = state.report[state.report.length - 1].id;
    statisticalInfo.id = lastIndex + 1;
    return {
      ...state,
      report: [...state.report, statisticalInfo],
    };
  }),
  on(updateStatisticalReport, (state, action) => {
    const updateStatistical = state.report.map((statiscticalInfo) => {
      return action.report.id === statiscticalInfo.id
        ? action.report
        : statiscticalInfo;
    });
    return {
      ...state,
      report: updateStatistical,
    };
  }),
  on(deleteStatisticalReport, (state, action) => {
    const updatestatisctical = state.report.filter((post) => {
      return post.id !== action.id;
    });
    return {
      ...state,
      report: updatestatisctical,
    };
  })
);

export function statisticalInfoReducer(state: any, action: any) {
  return _statisticalInfoReducer(state, action);
}
