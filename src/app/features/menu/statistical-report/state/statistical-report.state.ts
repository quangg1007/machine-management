import { statisticalReportType, type } from '../models/menu.models';

export interface staticReportState {
  report: statisticalReportType[];
}

export const STATISTICAL_REPORT_DATA: staticReportState = {
  report: [
    {
      id: 0,
      repairTicket: 'All Files.zip',
      productName: 'html',
      bill: 'html',
      productCode: 123,
      assign: 13579,
      type: 'fixing',
      date: '08 Jan 2019',
      button: '',
    },
    {
      id: 1,
      repairTicket: 'Header Photo.jpg',
      productName: 'css',
      bill: 'css',
      productCode: 345,
      assign: 8749857,
      type: 'newBuy',
      date: '08 Jan 2019',
      button: '',
    },
  ],
};

export const TYPE_VALUE_DATA: type[] = [
  {
    value: 'needToFix',
    viewValue: 'Cần sửa',
  },
  {
    value: 'newbuy',
    viewValue: 'Mua mới',
  },
  {
    value: 'maintenance',
    viewValue: 'Bảo trì',
  },
  {
    value: 'liquidation',
    viewValue: 'Thanh lý',
  },
  {
    value: 'fixing',
    viewValue: 'Đang sửa',
  },
];
