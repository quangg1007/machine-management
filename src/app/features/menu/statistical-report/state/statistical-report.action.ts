import { createAction, props } from '@ngrx/store';
import { statisticalReportType } from '../models/menu.models';

export const ADD_STATISTICAL_REPORT_ACTION =
  '[statistical report page] add statistical report';
export const UPDATE_STATISTICAL_REPORT_ACTION =
  '[statistical report page] update statistical report';
export const DELETE_STATISTICAL_REPORT_ACTION =
  '[statistical report page] delete statistical report';

export const addStatisticalReport = createAction(
  ADD_STATISTICAL_REPORT_ACTION,
  props<{ report: statisticalReportType }>()
);

export const updateStatisticalReport = createAction(
  UPDATE_STATISTICAL_REPORT_ACTION,
  props<{ report: statisticalReportType }>()
);

export const deleteStatisticalReport = createAction(
  DELETE_STATISTICAL_REPORT_ACTION,
  props<{ id: number }>()
);
