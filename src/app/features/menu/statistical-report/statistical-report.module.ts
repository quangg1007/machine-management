import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { PaginatorComponent } from '@app/shared/component';
import {
  EditStatisticalReportFormContainer,
  StatisticalReportContainer,
} from './container';
import { StatisticalReportRoutingModule } from './statistical-report-routing.module';
import { StatisticalReportView } from './view';
import { AddStatisticalReportFormContainer } from './container/add-statistical-report-form/add-statistical-report-form.container';
import { EditStatisticalReportFormView } from './view/edit-statistical-report-form/edit-statistical-report-form.view';
import { AddStatisticalReportFormView } from './view/add-statistical-report-form/add-statistical-report-form.view';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const containers = [
  StatisticalReportContainer,
  AddStatisticalReportFormContainer,
  EditStatisticalReportFormContainer,
];
const views = [
  StatisticalReportView,
  AddStatisticalReportFormView,
  EditStatisticalReportFormView,
];
const components: any[] = [];

@NgModule({
  declarations: [...containers, ...views, ...components],
  imports: [
    StatisticalReportRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PaginatorComponent,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatInputModule,
    MatSelectModule,
  ],
})
export class StatisticalReportModule {}
