import { Component } from '@angular/core';
import {
  statisticalReportDisplayType,
  statisticalReportType,
} from '../../models/menu.models';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { getStatisticalReport } from '../../state/statistical-report.selector';
import { getEmployee } from '@app/features/menu/employ-management/state/employ-management.selector';
import { employType } from '@app/features/menu/employ-management/models/menu.models';
import { deleteStatisticalReport } from '../../state/statistical-report.action';

@Component({
  selector: 'app-statistical-report-container',
  templateUrl: './statistical-report.container.html',
  styleUrls: ['./statistical-report.container.scss'],
})
export class StatisticalReportContainer {
  dataSource: statisticalReportDisplayType[] = [];
  statisticalReportInfo: Observable<statisticalReportType[]>;
  assignInfo: Observable<employType[]>;

  constructor(private _router: Router, private store: Store) {
    this.assignInfo = this.store.select(getEmployee);
    this.statisticalReportInfo = this.store.select(getStatisticalReport);
    this.statisticalReportInfo.subscribe((info) => {
      this.dataSource = [];
      info.map((info) => {
        this.assignInfo.subscribe((assign) => {
          assign.map((id) => {
            if (id.assign.employeeID === info.assign) {
              this.dataSource = [
                ...this.dataSource,
                {
                  id: info.id,
                  assign: id.assign,
                  repairTicket: info.repairTicket,
                  productName: info.productName,
                  bill: info.bill,
                  productCode: info.productCode,
                  type: info.type,
                  date: info.date,
                  button: '',
                },
              ];
            }
          });
        });
      });
    });
  }

  reportInfo(index: number): statisticalReportType | any {
    return {
      id: this.dataSource[index].id,
      repairTicket: this.dataSource[index].repairTicket,
      productName: this.dataSource[index].productName,
      bill: this.dataSource[index].bill,
      productCode: this.dataSource[index].productCode,
      assign: this.dataSource[index].assign,
      type: this.dataSource[index].type,
      date: this.dataSource[index].date,
      button: this.dataSource[index].button,
    };
  }

  routeToEdit(index: number): void {
    console.log(this.reportInfo(index));
    this._router.navigateByUrl('/home/danh-sach-bao-cao-thong-ke/chinh-sua', {
      state: this.reportInfo(index),
    });
  }

  deleteReport(id: number) {
    if (confirm('Bạn có chắc xóa thông tin!')) {
      console.log('delete the post');
    }

    this.store.dispatch(deleteStatisticalReport({ id: id }));
  }

  displayedColumns: string[] = [
    'position',
    'image',
    'name',
    'productCode',
    'assign',
    'tag',
    'date',
    'button',
  ];
}
