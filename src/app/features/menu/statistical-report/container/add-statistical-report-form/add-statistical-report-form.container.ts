import { LocationStrategy } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { addEmploy } from '@app/features/menu/employ-management/state/employ-management.actions';
import { AppState } from '@app/features/menu/store/app.state';
import { assignInfo } from '@app/features/menu/employ-management/models/menu.models';
import { getEmployee } from '@app/features/menu/employ-management/state/employ-management.selector';
import { statisticalReportType, type } from '../../models/menu.models';
import { addStatisticalReport } from '../../state/statistical-report.action';
import { TYPE_VALUE_DATA } from '../../state/statistical-report.state';

@Component({
  selector: 'app-add-statistical-report-form-container',
  templateUrl: './add-statistical-report-form.container.html',
  styleUrls: ['./add-statistical-report-form.container.scss'],
})
export class AddStatisticalReportFormContainer {
  reportForm: FormGroup;
  assignName: assignInfo[] = [];
  typeValue: type[] = TYPE_VALUE_DATA;
  constructor(
    private location: LocationStrategy,
    private fb: FormBuilder,
    private store: Store<AppState>
  ) {
    this.reportForm = this.fb.group({
      repairTicket: ['', [Validators.required]],
      productName: ['', [Validators.required]],
      bill: ['', [Validators.required]],
      productCode: ['', [Validators.required]],
      assign: ['', [Validators.required]],
      type: ['', [Validators.required]],
      date: ['', [Validators.required]],
    });
    this.store.select(getEmployee).subscribe((value) => {
      value.map((val, index) => {
        this.assignName[index] = val.assign;
      });
    });
  }

  back() {
    this.location.back();
  }

  onFormSubmit(): void {
    const reportInfo: statisticalReportType = {
      repairTicket: this.reportForm.value.repairTicket,
      productName: this.reportForm.value.productName,
      bill: this.reportForm.value.bill,
      productCode: this.reportForm.value.productCode,
      assign: this.reportForm.value.assign,
      type: this.reportForm.value.type,
      date: this.reportForm.value.date,
      button: this.reportForm.value.button,
    };
    console.log(reportInfo);
    this.store.dispatch(addStatisticalReport({ report: reportInfo }));
    this.location.back();
  }

  onFormClear(): void {}

  showNameError(prop: string) {
    const nameForm = this.reportForm.get(prop);
    if (nameForm?.touched && !nameForm.valid) {
      if (nameForm?.errors?.['required']) {
        return `${this.returnMessage(prop)} bắt buộc `;
      }
      if (nameForm?.errors?.['minlength']) {
        return `${this.returnMessage(prop)} nên có tối thiểu từ 10 kí tự`;
      }
    }
    return;
  }

  returnMessage(message: string): string {
    switch (message) {
      case 'repairTicket': {
        return 'Phiếu sửa chữa';
        break;
      }
      case 'productName': {
        return 'Tên thiết bị';
        break;
      }
      case 'bill': {
        return 'Hóa đơn';
        break;
      }
      case 'productCode': {
        return 'Mã sản phẩm';
        break;
      }
      case 'date': {
        return 'Thời gian';
        break;
      }
      default: {
        return '';
      }
    }
  }
}
