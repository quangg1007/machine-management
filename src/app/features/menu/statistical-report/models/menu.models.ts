import { assignInfo } from '../../employ-management/models/menu.models';

export interface statisticalReportType {
  id?: number;
  repairTicket: string;
  productName: string;
  bill: string;
  productCode: number;
  assign: number;
  type: string;
  date: string;
  button: string;
}

export interface statisticalReportDisplayType {
  id?: number;
  repairTicket: string;
  productName: string;
  bill: string;
  productCode: number;
  assign: assignInfo;
  type: string;
  date: string;
  button: string;
}

export interface type {
  value: string;
  viewValue: string;
}
