import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AddStatisticalReportFormView, StatisticalReportView } from './view';
import { EditStatisticalReportFormView } from './view/edit-statistical-report-form/edit-statistical-report-form.view';

const routes: Routes = [
  {
    path: '',
    component: StatisticalReportView,
  },
  {
    path: 'them-moi',
    component: AddStatisticalReportFormView,
  },
  {
    path: 'chinh-sua',
    component: EditStatisticalReportFormView,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StatisticalReportRoutingModule {}
